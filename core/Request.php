<?php
/**
 * Created by PhpStorm.
 * User: daw2
 * Date: 29/11/18
 * Time: 18:25
 */
namespace cursophp7\core;

class Request
{
    public static function uri()
    {
        return trim(
            parse_url($_SERVER['REQUEST_URI'],PHP_URL_PATH),
            '/'
        );
    }

    public static function method()
    {
        return $_SERVER['REQUEST_METHOD'];
    }
}