<?php
namespace cursophp7\app\exception;

use Exception;

class QueryException extends AppException
{
    public function __construct($message,  $code = 500)
    {
        parent::__construct($message, $code);
    }
}