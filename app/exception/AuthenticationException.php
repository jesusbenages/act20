<?php
/**
 * Created by PhpStorm.
 * User: daw2
 * Date: 17/12/18
 * Time: 15:46
 */

namespace cursophp7\app\exception;


class AuthenticationException extends AppException
{
    public function __construct($message,  $code = 403)
    {
        parent::__construct($message, $code);
    }
}