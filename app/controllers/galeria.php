<?php

use cursophp7\app\exception\AppException;
use cursophp7\app\exception\QueryException;
use cursophp7\app\repository\CategoriaRepository;
use cursophp7\app\repository\ImagenGaleriaRepository;
use cursophp7\core\App;


$errores=[];
$descripcion='';
$mensaje='';
//$imagenes=[];

try{

    $imagenes=App::getRepository(ImagenGaleriaRepository::class)->findAll();
    $categorias=App::getRepository(CategoriaRepository::class)->findAll();

}

catch(QueryException $queryException){
    $errores[]=$queryException->getMessage();
}
catch(AppException $appException){
    $errores[]=$appException->getMessage();
}

require __DIR__.'/../views/galeria.view.php';



?>