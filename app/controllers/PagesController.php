<?php
/**
 * Created by PhpStorm.
 * User: daw2
 * Date: 5/12/18
 * Time: 18:56
 */

namespace cursophp7\app\controllers;

use cursophp7\app\exception\QueryException;
use cursophp7\app\repository\AsociadoRepository;
use cursophp7\app\repository\ImagenGaleriaRepository;
use cursophp7\app\utils\Utils;
use cursophp7\core\App;
use cursophp7\core\Response;

class PagesController
{
    /**
     * @throws QueryException
     */
    public function index()
    {
        $imagenes=App::getRepository(ImagenGaleriaRepository::class)->findAll();
        $asociados=App::getRepository(AsociadoRepository::class)->findAll();

        Response::renderView(
            'index',
            'layout',
            compact(
                'imagenes',
                'asociados'
            )
        );

        //$asociados=Utils::extraerAsociados($asociados, 3);


    }

    public function about()
    {
        Response::renderView('about', 'layout-with-footer');

    }

    public function blog()
    {
        Response::renderView('blog', 'layout-with-footer');

    }

    public function post()
    {
        Response::renderView('single_post', 'layout-with-footer');

    }
}