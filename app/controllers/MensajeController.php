<?php
/**
 * Created by PhpStorm.
 * User: daw2
 * Date: 7/12/18
 * Time: 18:57
 */

namespace cursophp7\app\controllers;


use cursophp7\app\entity\Mensaje;
use cursophp7\app\exception\AppException;
use cursophp7\app\exception\QueryException;
use cursophp7\app\exception\ValidationException;
use cursophp7\app\repository\MensajeRepository;
use cursophp7\core\App;
use cursophp7\core\Response;

class MensajeController
{
    /**
     * @throws QueryException
     */
    public function index()
    {
        //$asociados=App::getRepository(MensajeRepository::class)->findAll();
        Response::renderView('contact');
           

    }

    /**
     * @throws QueryException
     * @throws AppException
     */
    public function nuevo()
    {
        try{

            $nombre = trim(htmlspecialchars($_POST['nombre']));
            $apellido = trim(htmlspecialchars($_POST['apellido']));
            $email = trim(htmlspecialchars($_POST['email']));
            $tema = trim(htmlspecialchars($_POST['tema']));
            $texto = trim(htmlspecialchars($_POST['mensaje']));


            if (empty($nombre)) {
                throw new ValidationException('El nombre no puede quedar vacío');
            }
            if (empty($email)) {
                throw new ValidationException('El e-mail no puede quedar vacío');

            } else {
                if (filter_var($email, FILTER_VALIDATE_EMAIL) === false) {

                    throw new ValidationException('El email no es válido');

                }
            }
            if (empty($tema)) {
                throw new ValidationException('El asunto no puede quedar vacío');

            }
            if (empty($errores)) {

                $mensaje=new Mensaje($nombre, $apellido,$tema,$email,$texto);

                App::getRepository(MensajeRepository::class)->save($mensaje);
                $message="Se ha guardado un nuevo mensaje" . $mensaje->getTexto();
                App::get('logger')->add($message);


            }

        }catch(ValidationException $validationException){

            die($validationException->getMessage());
        }
        App::get('router')->redirect('contact');

    }

}